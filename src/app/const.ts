export class CONST {
  constructor() { }

  public static BASE_URL = 'http://localhost';
  public static PORT = '8001';
  public static API_URL = `${CONST.BASE_URL}:${CONST.PORT}/api/`; // VEZI CA NU MERE PE HTTPS !!!!!111!!
  public static UPLOADS_URL = `${CONST.BASE_URL}/uploads`;

  public static NOTIFICATIONS_REFRESH_INTERVAL = 30;

  public static DEFAULT_SETTINGS: any = {
    sound: true,
    vibration: false
  };

  public USER: IUser;
}

// Interfaces
export interface IUser {
  error?: string;
  level: string;
  role: string;
  token: string;
  user_fullname: string;
  user_session: string;
  session: string;
  image: string;
}

export interface IRemeber {
  error?: string;
  username: string;
  password: string;
}
