import { Component, OnInit, Inject, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { CONST } from '../const';
import { IUser, IRemeber } from '../const';
import { UserProvider } from '../../providers/user/user';

export interface DialogData {
  submitted: number;
}

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  dataSource: Array<any> = [];
  yourTask: Array<any> = [];
  allTask: Array<any> = [];
  console = console;
  yourTaskError = false;
  allTaskError = false;

  progressReport: Array<any> = [];

  constructor(private http: HttpClient, public dialog: MatDialog, private overlay: Overlay, private userProvider: UserProvider) {
  }

  ngOnInit() {
    this.getYourTasks();
    this.getAllTasks();
  }

  getYourTasks(): void {
    this.yourTask = [];
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-tasks', params).pipe(
      map((resp: any) => {
        return Object.keys(resp)
          .map((key: any) => {
            const obj: any = resp[key];
            return obj;
          });
      })
    )
    .subscribe((resp: any) => {
      Object.keys(resp)
        .map((key: any) => {
          this.yourTask = resp;
        });
      this.yourTaskError = false;
    }, (error: any) => {
      this.yourTaskError = true;
    }
    );
  }

  getAllTasks(): void {
    this.allTask = [];
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-all-tasks', params).pipe(
      map((resp: any) => {
        return Object.keys(resp)
          .map((key: any) => {
            const obj: any = resp[key];
            return obj;
          });
      })
    )
    .subscribe((resp: any) => {
      Object.keys(resp)
        .map((key: any) => {
          this.allTask = resp;
        });
      this.allTaskError = false;
    }, (error: any) => {
      this.allTaskError = true;
    }
    );
  }

  addTask(): void {
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    const dialogRef = this.dialog.open(AddTaskComponent, {
      data: {
      },
      autoFocus: false,
      scrollStrategy
    });

    dialogRef.afterClosed().subscribe(
      data =>  {
        return data === 1 ? [this.getYourTasks(), this.getAllTasks()] : '';
      }
    );
  }

}

@Component({
  selector: 'app-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  projectIds: Array<any> = [];

  constructor(
    public dialogRef: MatDialogRef<AddTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private http: HttpClient, private formBuilder: FormBuilder,
    private userProvider: UserProvider) {
      dialogRef.disableClose = true;
      this.data.submitted = 0;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      project_id: ['', Validators.required],
    });
    this.secondFormGroup = this.formBuilder.group({
      task_name: [''],
      task_description: [''],
    });
    this.getProjectsForSelect();
  }

  getProjectsForSelect(): void {
    this.projectIds = [];
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-all-projects-for-select', params).pipe(
      map((resp: any) => {
        return Object.keys(resp)
          .map((key: any) => {
            const obj: any = resp[key];
            return obj;
          });
      })
    )
    .subscribe((resp: any) => {
      Object.keys(resp)
        .map((key: any) => {
          this.projectIds = resp;
        });
    }, (error: any) => {
    }
    );
  }

  submitTask(): void {
    this.data.submitted = 0;
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session)
      .set('project_id', this.firstFormGroup.controls.project_id.value)
      .set('task_name', this.secondFormGroup.controls.task_name.value)
      .set('task_description', this.secondFormGroup.controls.task_description.value);
    this.http.post(CONST.API_URL + 'add-task', params).pipe(
      map((resp: any) => {
          if (resp.status === 'ok') {
            return resp;
          }
        })
      )
      .subscribe((resp: any) => {
        this.data.submitted = 1;
        this.dialogRef.close(this.data.submitted);
      }, (error: any) => {
        this.dialogRef.close(this.data.submitted);
      });
  }
}
