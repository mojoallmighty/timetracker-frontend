import { Component, OnInit, Inject, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { CONST } from '../const';
import { IUser, IRemeber } from '../const';
import { UserProvider } from '../../providers/user/user';

export interface DialogData {
  submitted: number;
}

type UserType = Array<{name?: string, user_id: string}>;
type InvolvedUserType = Array<{name?: string, working_hours: string}>;
type TaskType = Array<{task_name?: string, task_id: string, participating: UserType, created_by: UserType}>;
type ProjectType = Array<{project_name?: string, project_description: string, project_id: string, task: TaskType, created_by: string,
  people_involved: InvolvedUserType}>;


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  dataSource: Array<any> = [];
  yourProjects: ProjectType = [];
  allProjects: ProjectType = [];
  console = console;
  yourProjectsError = false;
  allProjectsError = false;

  progressReport: Array<any> = [];

  constructor(private http: HttpClient, public dialog: MatDialog, private overlay: Overlay, private userProvider: UserProvider) {
  }

  ngOnInit() {
    this.getYourProjects();
    this.getAllProjects();
  }

  getYourProjects(): void {
    this.yourProjects = [];
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-projects', params).pipe(
      map((resp: any) => {
        return Object.keys(resp)
          .map((key: any) => {
            const obj: any = resp[key];
            return obj;
          });
      })
    )
    .subscribe((resp: any) => {
      Object.keys(resp)
        .map((key: any) => {
          const tasks: any = [];
          const peopleInvolved: any = [];
          Object.keys(resp[key].tasks)
            .map((k: any) => {
              tasks.push(resp[key].tasks[k]);
            });
          Object.keys(resp[key].people_involved)
            .map((k: any) => {
              peopleInvolved.push(resp[key].people_involved[k]);
            });
          this.yourProjects.push({
            project_name: resp[key].name,
            project_description: resp[key].description,
            project_id: resp[key].id,
            task: tasks,
            created_by: resp[key].created_by,
            people_involved: peopleInvolved
          });
        });
      this.yourProjectsError = false;
    }, (error: any) => {
      this.yourProjectsError = true;
    }
    );
  }

  getAllProjects(): void {
    this.allProjects = [];
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-all-projects', params).pipe(
      map((resp: any) => {
        return Object.keys(resp)
          .map((key: any) => {
            const obj: any = resp[key];
            return obj;
          });
      })
    )
    .subscribe((resp: any) => {
      Object.keys(resp)
        .map((key: any) => {
          const tasks: any = [];
          const peopleInvolved: any = [];
          Object.keys(resp[key].tasks)
            .map((k: any) => {
              tasks.push(resp[key].tasks[k]);
            });
          Object.keys(resp[key].people_involved)
            .map((k: any) => {
              peopleInvolved.push(resp[key].people_involved[k]);
            });
          this.allProjects.push({
            project_name: resp[key].name,
            project_description: resp[key].description,
            project_id: resp[key].id,
            task: tasks,
            created_by: resp[key].created_by,
            people_involved: peopleInvolved
          });
        });
      this.allProjectsError = false;
    }, (error: any) => {
      this.allProjectsError = true;
    }
    );
  }

  addProject(): void {
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    const dialogRef = this.dialog.open(AddProjectComponent, {
      data: {
      },
      autoFocus: false,
      scrollStrategy
    });

    dialogRef.afterClosed().subscribe(
      data =>  {
        return data === 1 ? [this.getAllProjects(), this.getYourProjects()] : '';
      }
    );
  }

}

@Component({
  selector: 'app-add-project',
  templateUrl: 'add-project.html',
})
export class AddProjectComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private http: HttpClient, private formBuilder: FormBuilder,
    private userProvider: UserProvider) {
      dialogRef.disableClose = true;
      this.data.submitted = 0;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      project_name: ['', Validators.required],
    });
    this.secondFormGroup = this.formBuilder.group({
      project_description: ['', Validators.required],
    });
    this.thirdFormGroup = this.formBuilder.group({
      task_name: [''],
      task_description: [''],
    });
  }

  submitProject(): void {
    this.data.submitted = 0;
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session)
      .set('project_name', this.firstFormGroup.controls.project_name.value)
      .set('project_description', this.secondFormGroup.controls.project_description.value)
      .set('task_name', this.thirdFormGroup.controls.task_name.value)
      .set('task_description', this.thirdFormGroup.controls.task_description.value);
    this.http.post(CONST.API_URL + 'add-project', params).pipe(
      map((resp: any) => {
          if (resp.status === 'ok') {
            return resp;
          }
        })
      )
      .subscribe((resp: any) => {
        this.data.submitted = 1;
        this.dialogRef.close(this.data.submitted);
      }, (error: any) => {
        this.dialogRef.close(this.data.submitted);
      });
  }
}
