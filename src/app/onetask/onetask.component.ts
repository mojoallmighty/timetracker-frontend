import { Component, OnInit, Inject, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { CONST } from '../const';
import { IUser, IRemeber } from '../const';
import { UserProvider } from '../../providers/user/user';


type UserType = Array<{name?: string, user_id: string}>;
type InvolvedUserType = Array<{name?: string, working_hours: string}>;
// type TaskType = Array<{task_name?: string, task_id: string, participating: UserType, created_by: UserType,
//  people_involved: InvolvedUserType}>;
type TaskType = Array<{task_name?: string, task_id: string, total_worked_hours: string, personal_worked_hours: string, task: any,
  involved_people: string}>;

export interface DialogData {
  task: string;
  submitted: number;
}

@Component({
  selector: 'app-onetask',
  templateUrl: './onetask.component.html',
  styleUrls: ['./onetask.component.css']
})

export class OnetaskComponent implements OnInit {
  taskId: any = '';
  tasksType: TaskType = [];
  tsks: Array<any> = [];
  console = console;
  tasksError = false;

  constructor(private activatedRoute: ActivatedRoute, private userProvider: UserProvider, private http: HttpClient,
              public dialog: MatDialog, private overlay: Overlay) {
    activatedRoute.params.subscribe(
      params => this.taskId = params.task_id);

    console.log(this.taskId);
  }

  ngOnInit() {
    this.getTask();
  }

  getTask(): void {
    this.tsks = [];
    const params: HttpParams = new HttpParams()
      .set('id', this.taskId)
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-task-by-id', params).pipe(
    )
    .subscribe((resp: any) => {
      const task: any = [];
      const peopleInvolved: any = [];
      if (resp.task.length > 0) {
        Object.keys(resp.task)
          .map((k: any) => {
            task.push(resp.task[k]);
          });
      }
      this.tasksType.push({
        task_name: resp.task_name,
        task_id: this.taskId,
        total_worked_hours: resp.total_worked_hours,
        personal_worked_hours: resp.personal_worked_hours,
        task,
        involved_people: resp.involved_people
      });
      this.tasksType.map((r: any) => {
        this.tsks = r;
      });
      this.tasksError = false;
    }, (error: any) => {
      this.tasksError = true;
    }
    );
  }

  addHour(): void {
    const scrollStrategy = this.overlay.scrollStrategies.reposition();
    const dialogRef = this.dialog.open(AddHourComponent, {
      data: {
        task: this.taskId
      },
      autoFocus: false,
      scrollStrategy
    });

    dialogRef.afterClosed().subscribe(
      data =>  {
        return data === 1 ? this.getTask() : '';
      }
    );
  }

}

@Component({
  selector: 'app-add-hour',
  templateUrl: 'add-hour.html',
})
export class AddHourComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddHourComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private http: HttpClient, private formBuilder: FormBuilder,
    private userProvider: UserProvider) {
      dialogRef.disableClose = true;
      this.data.submitted = 0;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      hours: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  submitHour(): void {
    this.data.submitted = 0;
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session)
      .set('task_id', this.data.task)
      .set('description', this.formGroup.controls.description.value)
      .set('hours', this.formGroup.controls.hours.value);
    this.http.post(CONST.API_URL + 'add-hour', params).pipe(
      map((resp: any) => {
          if (resp.status === 'ok') {
            return resp;
          }
        })
      )
      .subscribe((resp: any) => {
        this.data.submitted = 1;
        this.dialogRef.close(this.data.submitted);
      }, (error: any) => {
        this.dialogRef.close(this.data.submitted);
      });
  }
}

