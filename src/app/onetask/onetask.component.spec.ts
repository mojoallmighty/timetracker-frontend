import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnetaskComponent } from './onetask.component';

describe('OnetaskComponent', () => {
  let component: OnetaskComponent;
  let fixture: ComponentFixture<OnetaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnetaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnetaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
