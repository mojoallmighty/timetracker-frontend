import {MediaMatcher} from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UserProvider } from '../../providers/user/user';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnDestroy {
  opened: boolean;

  mobileQuery: MediaQueryList;

  private mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private router: Router, public userProvider: UserProvider) {
  this.mobileQuery = media.matchMedia('(max-width: 600px)');
  this.mobileQueryListener = () => changeDetectorRef.detectChanges();
  this.mobileQuery.addListener(this.mobileQueryListener);
}

ngOnDestroy(): void {
  this.mobileQuery.removeListener(this.mobileQueryListener);
}

toggleMenu(menuOption: string) {
  this[menuOption] = !this[menuOption];
  }

  makeActive($e) {
    console.log($e.target);
  }

  logout(): void {
    localStorage.removeItem('user');
    location.reload();
  }

}
