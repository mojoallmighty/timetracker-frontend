import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StarRatingModule} from 'angular-star-rating';
import {MaterialModule} from './material-module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { FirstComponent } from './first/first.component';
import {RouterModule, Routes} from '@angular/router';
import {CustomMaterialModule} from './core/material.module';

import { UserProvider } from '../providers/user/user';
import { DatabaseProvider } from '../providers/database/database';
import { CONST } from './const';

import { LoginPageComponent } from './login/login.component';
import { ProjectsComponent, AddProjectComponent } from './projects/projects.component';
import { TasksComponent, AddTaskComponent } from './tasks/tasks.component';
import { OnetaskComponent, AddHourComponent } from './onetask/onetask.component';

const appRoutes: Routes = [
  { path: '', component: FirstComponent, data: { title: 'Home' } },
  { path: 'projects', component: ProjectsComponent, data: { title: 'Projects' } },
  { path: 'tasks', component: TasksComponent, data: { title: 'Tasks' } },
  { path: 'tasks/:task_id', component: OnetaskComponent, data: { title: 'Tasks' } },
  { path: 'login', component: LoginPageComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    NavigationComponent,
    FirstComponent,
    ProjectsComponent,
    AddProjectComponent,
    TasksComponent,
    AddTaskComponent,
    OnetaskComponent,
    AddHourComponent
  ],
  entryComponents: [
    LoginPageComponent,
    AddProjectComponent,
    OnetaskComponent,
    AddHourComponent,
    AddTaskComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: false } // <-- debugging purposes only
    ),
    CustomMaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    StarRatingModule.forRoot()
  ],
  exports: [
    BrowserModule,
    BrowserAnimationsModule,
  ],
  providers: [
    UserProvider,
    DatabaseProvider,
    LoginPageComponent,
    CONST
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

