import { Component, OnInit, Inject, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { CONST } from '../const';
import { IUser, IRemeber } from '../const';
import { UserProvider } from '../../providers/user/user';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  dataSource: Array<any> = [];
  week: Array<any> = [];
  month: Array<any> = [];
  console = console;
  weekError = false;
  monthError = false;

  progressReport: Array<any> = [];

  constructor(private http: HttpClient, public dialog: MatDialog, private overlay: Overlay, private userProvider: UserProvider) {
  }

  ngOnInit() {
    this.getWeek();
    this.getMonth();
  }

  getWeek(): void {
    this.week = [];
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-week-homepage', params).pipe()
    .subscribe((resp: any) => {
      Object.keys(resp)
        .map((key: any) => {
          this.week = resp;
        });
      this.weekError = false;
    }, (error: any) => {
      this.weekError = true;
    }
    );
  }

  getMonth(): void {
    this.month = [];
    const params: HttpParams = new HttpParams()
      .set('user_session', this.userProvider.getUser().user_session);

    this.http.post(CONST.API_URL + 'get-month-homepage', params).pipe()
    .subscribe((resp: any) => {
      Object.keys(resp)
        .map((key: any) => {
          this.month = resp;
        });
      this.monthError = false;
    }, (error: any) => {
      this.monthError = true;
    }
    );
  }

}
