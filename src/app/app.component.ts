import { Component, OnInit } from '@angular/core';
import { Utils } from './utils';
import { UserProvider } from '../providers/user/user';
import { IUser, IRemeber } from './const';
import { DatabaseProvider } from '../providers/database/database';
import {RouterModule, Routes} from '@angular/router';
import { LoginPageComponent } from './login/login.component';
import { CONST } from './const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  console = console;
  nouser = false;

  constructor(public userProvider: UserProvider, private router: RouterModule) {}

  ngOnInit(): void {
    const resp = {
      user_first_name: 'Test',
      user_last_name: 'Test',
      user_session: '213',
      session: '123',
      image: '',
    };
    // localStorage.setItem('user', JSON.stringify(resp));

    if ( this.userProvider.getUser() === null || Object.keys(this.userProvider.getUser()).length < 1) {
      this.nouser = true;
    }
  }
}
