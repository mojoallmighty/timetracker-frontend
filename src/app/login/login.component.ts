import { Component, OnInit, Inject } from '@angular/core';
import { UserProvider } from '../../providers/user/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CONST, IUser, IRemeber } from '../const';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginPageComponent implements OnInit {

  public loginForm: FormGroup;
  public loading: any;
  public userExists = false;

  public buttonClicked = false;
  public passwordShown = false;

  private remember: IRemeber = {} as IRemeber;

  constructor( public userProvider: UserProvider, private formBuilder: FormBuilder,
               private dialog: MatDialog, private router: Router, private http: HttpClient) {

    this.loginForm = this.formBuilder.group({
      email: ['',
        [Validators.required, Validators.email, Validators.minLength(4), Validators.maxLength(40)]],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(40)]],
      rememberMe: [false],
    });

  }

  checkUsername(): void {
    const params: HttpParams = new HttpParams()
      .set('username', this.loginForm.controls.email.value);

    this.http.post(CONST.API_URL + 'check-username', params)
    .subscribe((resp: any) => {
      console.log(resp);
      if (resp.error) {
        console.log('this');
        let errorMsg: string;
        switch (resp.error) {
          case 'login': {
            errorMsg = 'Email or username incorect!';
            break;
          }
          case 'email': {
            errorMsg = 'Please verify your email address!';
            break;
          }
          case 'disabled': {
            errorMsg = 'This account has been disabled!';
            break;
          }
          default: {
            errorMsg = 'Login failed!';
            break;
          }
        }
      } else if (resp.status === 'ok') {
        this.userExists = true;
        this.passwordShown = true;
      }
    }, (error: any) => {
    }
    );
  }

  login(): void {
    this.buttonClicked = !this.buttonClicked;
    const obs: any = this.userProvider.login(this.loginForm.controls.email.value,
      this.loginForm.controls.password.value);
    obs.subscribe((resp: any) => {
      if (resp.error) {
        let errorMsg: string;
        switch (resp.error) {
          case 'login': {
            errorMsg = 'Email or password incorect!';
            break;
          }
          case 'email': {
            errorMsg = 'Please verify your email address!';
            break;
          }
          case 'disabled': {
            errorMsg = 'This account has been disabled!';
            break;
          }
          default: {
            errorMsg = 'Login failed!';
            break;
          }
        }
        this.buttonClicked = false;
      } else if (!resp) {
        this.buttonClicked = false;
      } else {
        localStorage.setItem('user', JSON.stringify(resp));
        location.reload();
      }
    }, (error: any) => {
      this.buttonClicked = false;
    });
  }

  forgotPassword(): void {

  }

  ngOnInit(): void {
    this.router.navigate([this.router.url]);
  }

}
