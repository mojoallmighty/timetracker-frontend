export class Utils {

  static DEBUG_ENABLED = true;

  static log(...data: Array<any>): void {
    if (this.DEBUG_ENABLED) {
      Utils.log = console.log.bind(window.console);
      Utils.log(...data);
    }
  }
  static warn(...data: Array<any>): void {
    if (this.DEBUG_ENABLED) {
      Utils.warn = console.warn.bind(window.console);
      Utils.warn(...data);
    }
  }
  static error(...data: Array<any>): void {
    if (this.DEBUG_ENABLED) {
      Utils.error = console.error.bind(window.console);
      Utils.error(...data);
    }
  }
}
