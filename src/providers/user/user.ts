import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONST, IUser, IRemeber } from '../../app/const';
import { Observable } from 'rxjs/Observable';
import { DatabaseProvider } from '../database/database';
import 'rxjs/add/operator/share';

@Injectable()
export class UserProvider {

  private user: IUser = {} as IUser;
  private remember: IRemeber = {} as IRemeber;

  constructor(public http: HttpClient, private db: DatabaseProvider) {
  }

  login(email: string, password: string): Observable<any> {
    const params: HttpParams = new HttpParams()
      .set('email', email)
      .set('password', password);

    return this.http.post(CONST.API_URL + 'login', params);
  }

  resetPassword(email: string): Observable<any> {
    const params: HttpParams = new HttpParams()
      .set('source', 'webpage')
      .set('scope', 'forgot-password')
      .set('username', email);

    return this.http.post(CONST.API_URL, params);
  }

  loadUser(): Promise<any> {
    const promiseLoadUser: Promise<any> = new Promise((resolve: any) => {
      this.user = JSON.parse(localStorage.getItem('user'));
    });
    return promiseLoadUser;

  }

  getUser(): IUser {
    this.loadUser().then((user: IUser) => {
      this.user = user;
      console.log(this.user);
    });

    return this.user;
  }

}
