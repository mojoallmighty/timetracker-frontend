import { Injectable } from '@angular/core';
import { Utils } from '../../app/utils';
import { CONST } from '../../app/const';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Injectable()
export class DatabaseProvider {

  constructor(protected storage: LocalStorage) {
  }

  public read(key: string): Promise<any> {
    return  new Promise((resolve: any) => {
      return JSON.parse(localStorage.getItem(key));
    });
  }
}
